<?php

$plugin = [
  'title' => t('Bitbucket Git history synchronizer'),
  'worker' => [
    'class' => 'VersioncontroliBitbucketGitRepositoryHistorySynchronizer',
  ],
];
