<?php

$plugin = array(
  'title' => t('Bitbucket Git repository manager'),
  'worker' => array(
    'class' => 'VersioncontrolBitbucketGitRepositoryManagerWorker',
  ),
);
