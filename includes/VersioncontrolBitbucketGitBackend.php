<?php

class VersioncontrolBitbucketGitBackend extends VersioncontrolGitBackend {

  public $type = 'bitbucket_git';

  public function __construct() {
    parent::__construct();
    $this->name = 'Bitbucket Git';
    $this->description = t('Bitbucket Git integrates Bitbucket throught the REST API.');
  }

  /**
   * Provide default plugins.
   */
  public function getDefaultPluginName($plugin_type) {
    // Use our reposync and repomgr provided plugin as default.
    if ($plugin_type == 'reposync' || $plugin_type == 'repomgr') {
      return 'bitbucket_git';
    }
  }

}
